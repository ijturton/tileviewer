package com.ianturton.tileviewer;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;
import org.geotools.coverage.GridSampleDimension;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.io.AbstractGridCoverage2DReader;
import org.geotools.coverage.grid.io.AbstractGridFormat;
import org.geotools.coverage.grid.io.GridFormatFactorySpi;
import org.geotools.coverage.grid.io.GridFormatFinder;
import org.geotools.data.DataStoreFactorySpi;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.DataAccessFactory.Param;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.wms.WebMapServer;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.factory.Hints;
import org.geotools.gce.geotiff.GeoTiffFormat;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.FeatureLayer;
import org.geotools.map.GridCoverageLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.map.MapViewport;
import org.geotools.map.WMSLayer;
import org.geotools.ows.ServiceException;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.styling.ChannelSelection;
import org.geotools.styling.ContrastEnhancement;
import org.geotools.styling.ContrastEnhancementImpl;
import org.geotools.styling.RasterSymbolizer;
import org.geotools.styling.SLD;
import org.geotools.styling.SelectedChannelType;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.geotools.styling.StyledLayerDescriptor;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.geotools.swing.wms.WMSChooser;
import org.geotools.swing.wms.WMSLayerChooser;
import org.geotools.tile.impl.WebMercatorTileService;
import org.geotools.tile.impl.bing.BingService;
import org.geotools.tile.impl.osm.OSMService;
import org.geotools.tile.util.TileLayer;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.FilterFactory;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.cs.AxisDirection;
import org.opengis.referencing.operation.TransformException;
import org.opengis.style.ContrastMethod;
import org.xml.sax.SAXException;

public class TileViewer {
  private final static FilterFactory ff = CommonFactoryFinder.getFilterFactory2();
  private final static StyleFactory sf = CommonFactoryFinder.getStyleFactory();
  private static final int RED = 0;
  private static final int GREEN = 1;
  private static final int BLUE = 2;

  JMapFrame frame = new JMapFrame();
  /* Create the Open Street Map Service */
  OSMService osm = new OSMService("OSM", "http://tile.openstreetmap.org/");
  
  private MapContent content = new MapContent();
  /* EPSG:4326 lat/lon crs*/
  static CoordinateReferenceSystem crs;
  static CoordinateReferenceSystem latlon = DefaultGeographicCRS.WGS84;
  /* EPSG:3857 Web mercator crs for OSM tiles */
  static CoordinateReferenceSystem mercator;
  static {
    try {
      mercator = CRS.decode("EPSG:3857");
      crs = mercator;
    } catch (FactoryException e) {
      e.printStackTrace();

    }
  }
  
  /* Due to the constraints of the Web Mercator Projection we can't request 
   * tiles above or below 85N/S, so set the bounds to reflect this limit.
   */
  
  ReferencedEnvelope bbox = new ReferencedEnvelope(WebMercatorTileService.MIN_LONGITUDE,
      WebMercatorTileService.MAX_LONGITUDE, WebMercatorTileService.MIN_LATITUDE, WebMercatorTileService.MAX_LATITUDE,
      latlon);
  

  public TileViewer(String[] args) throws ParseException {
    Options options = new Options();

    options.addOption(null, "help", false, "Print this help message");
    options.addOption(null, "crs", true, "The EPSG code of the coordinate reference system needed");

    options.addOption("b", "bbox", true, "the bounding box of the map (minX, minY, maxX, maxY) quoted");
    CommandLineParser parser = new DefaultParser();

    CommandLine commandLine = parser.parse(options, args);

    if (commandLine.hasOption("crs")) {
      setProjection(commandLine.getOptionValue("crs"));
    } else {
      crs = mercator;
    }
    if (commandLine.hasOption("bbox")) {
      setBbox(commandLine.getOptionValue("bbox"));
    }

    if (commandLine.hasOption("help")) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("java -jar "
          + new java.io.File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getName(),
          options);
      System.exit(1);
    }
    /*
     * The viewport is the area of the map that will be displayed on the screen.
     */
    MapViewport viewport = content.getViewport();
    viewport.setCoordinateReferenceSystem(crs);
    try {
      viewport.setBounds(bbox.transform(crs, true));
    } catch (TransformException | FactoryException e) {

      e.printStackTrace();
    }
    /*
     * The MapContent controls what is drawn on the screen.
     * Here we add a new Open Street Map layer.
     */
    content.addLayer(new TileLayer(osm));
    /*
     * Create menu and items.
     */
    JMenuBar menuBar = new JMenuBar();
    JMenu boundsMenu = new JMenu("Bounds");
    boundsMenu.setMnemonic('B');
    JMenuItem bounds = new JMenuItem("Set Bounding Box");
    bounds.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ReferencedEnvelope env = null;
        try {
          /*
           * convert the bounds of the view back to lat/lon for display.
           */
          env = viewport.getBounds().transform(latlon, true);
        } catch (TransformException | FactoryException e2) {
          e2.printStackTrace();
          return;
        }
        JTextField north = new JTextField(""+env.getMaxY());
        JTextField south = new JTextField(""+env.getMinY());
        JTextField east = new JTextField(""+env.getMaxX());
        JTextField west = new JTextField(""+env.getMinX());

        JPanel panel = new JPanel(new GridLayout(0, 2));
        panel.add(new JLabel("Set Bounding Box in Degrees"));
        panel.add(new JLabel(""));
        panel.add(new JLabel("North:"));
        panel.add(north);
        
        panel.add(new JLabel("West:"));
        panel.add(west);
        panel.add(new JLabel("East:"));
        panel.add(east);
        panel.add(new JLabel("South:"));
        panel.add(south);
        int result = JOptionPane.showConfirmDialog(frame, panel, "Set BBOX", JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
          /*
           * create the lat/lon box, and then convert it to the projection of the
           * viewport (the map) and update it.
           */
          ReferencedEnvelope box = new ReferencedEnvelope(Double.parseDouble(west.getText()),
              Double.parseDouble(east.getText()), Double.parseDouble(north.getText()),
              Double.parseDouble(south.getText()), latlon);
          viewport.setMatchingAspectRatio(true);
          try {
            viewport.setBounds(box.transform(crs, true));
            frame.getMapPane().setDisplayArea(box.transform(crs, true));
          } catch (TransformException | FactoryException e1) {
            e1.printStackTrace();
          }

        } else {
          System.out.println("Cancelled");
        }
      }
    });
    boundsMenu.add(bounds);
    JMenu fileMenu = new JMenu("File");
    fileMenu.setMnemonic('F');
    JMenuItem addFile = new JMenuItem("Add ShapeFile");
    addFile.setMnemonic('F');
    addFile.addActionListener(new ActionListener() {
      File file = null;

      @Override
      public void actionPerformed(ActionEvent e) {

        file = JFileDataStoreChooser.showOpenFile("shp", null);
        if (file == null) {
          return;
        }

        addVectorfile(file);
      }
    });
    fileMenu.add(addFile);

    JMenuItem addRaster = new JMenuItem("Add Raster");
    addRaster.setMnemonic('R');
    addRaster.addActionListener(new ActionListener() {
      File file = null;

      @Override
      public void actionPerformed(ActionEvent e) {

        file = JFileDataStoreChooser.showOpenFile("", null);
        if (file == null) {
          return;
        }

        addCoverage(file);
      }
    });
    fileMenu.add(addRaster);

    JMenuItem addWMS = new JMenuItem("Add WMS");
    addWMS.setMnemonic('W');
    addWMS.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        /*
         * Ask the user to enter a GetCapabilities URL 
         */
        URL capabilitiesURL = WMSChooser.showChooseWMS();
        if (capabilitiesURL == null) {
          return;
        }
        WebMapServer wms = null;
        try {
          //create a WMS Service 
          wms = new WebMapServer(capabilitiesURL);
        } catch (ServiceException | IOException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
          return;
        }
        //show a list of available layers for the user to choose.
        List<org.geotools.data.ows.Layer> wmsLayers = WMSLayerChooser.showSelectLayer(wms);
        if (wmsLayers == null) {
          JOptionPane.showMessageDialog(null, "Could not connect - check url");
          return; 
        }
        // For each layer chosen, create a mapLayer and add it to the map.
        for (org.geotools.data.ows.Layer wmsLayer : wmsLayers) {
          WMSLayer displayLayer = new WMSLayer(wms, wmsLayer);
          content.addLayer(displayLayer);
        }
      }
    });
    fileMenu.add(addWMS);
    menuBar.add(fileMenu);
    menuBar.add(boundsMenu);
    frame.setJMenuBar(menuBar);
    frame.setSize(700, 700);

    frame.enableToolBar(true);
    frame.enableStatusBar(true);
    frame.setMapContent(content);

  }

  /**
   * Reads a raster (coverage) file and adds it to the mapcontent.
   * Applies a default style based on the number of bands and a guess as to RGB.
   * 
   * @param f the file to read.
   */
  public void addCoverage(File f) {
    AbstractGridFormat format = GridFormatFinder.findFormat(f);
    // working around a bug/quirk in geotiff loading via format.getReader which
    // doesn't set this correctly
    Hints hints = null;
    if (format instanceof GeoTiffFormat) {
      hints = new Hints(Hints.FORCE_LONGITUDE_FIRST_AXIS_ORDER, Boolean.TRUE);
    }

    /* Work out what sort of reader is needed for this file type.
     * Which types can be read is determined by which jars are on the class path.
     *
     *To check what is available use code like:
     *
     *  private static Map<String, GridFormatFactorySpi> formats = new HashMap<>();
     *   if (formats.isEmpty()) {
     *     Set<GridFormatFactorySpi> fmts = GridFormatFinder.getAvailableFormats();
     *     for (GridFormatFactorySpi spi : fmts) {
     *       AbstractGridFormat format = spi.createFormat();
     *       formats.put(format.getName().toLowerCase(), spi);
     *     }
     *  }
     */
    AbstractGridCoverage2DReader reader = format.getReader(f, hints);

    GridCoverage2D cov = null;
    try {
      //read in the coverage
      cov = reader.read(null);
    } catch (IOException giveUp) {
      giveUp.printStackTrace();
      return;
    }
    if (cov == null) {
      System.out.println("no coverage found");
      return;
    }
    // generate a default style
    Style rasterStyle = createRGBStyle(cov);
    //create a raster layer with the styled data 
    Layer rasterLayer = new GridCoverageLayer(cov, rasterStyle);
    if (rasterLayer.getBounds() == null) {
      System.out.println("bad raster layer ");
      return;
    }
    // add the styled data to the map.
    content.addLayer(rasterLayer);
  }

  /**
   * Loads a file and matching sld file if present and adds to the map.
   * Supported types depends on included dependencies.
   * 
   * @param f file to be read
   * 
   */

  /* 
   * To find which vector datastores are available and which parameters they need
   * use the following code.
   * 
   * Iterator<DataStoreFactorySpi> it = DataStoreFinder.getAllDataStores();
   * while (it.hasNext()) {
   *  DataStoreFactorySpi fac = it.next();
   *  String name = fac.getDisplayName();
   *  String desc = fac.getDescription();
   *  Param[] params = fac.getParametersInfo();
   *  
   *  System.out.println("DataStoreFactory "+name);
   *  System.out.println(desc);
   *  System.out.println("Parameters:");
   *  for(Param param:params) {
   *      System.out.print("\tkey='"+param.key+"' ");
   *      if(param.isRequired()) {
   *        System.out.print("(required) ");
   *      }
   *      System.out.print(param.getDescription());
   *      if(param.getDefaultValue()!=null) {
   *        System.out.print(" default=["+param.getDefaultValue()+"]");
   *      }
   *      System.out.println();
   *  }
   * }
   */
  public void addVectorfile(File f) {
    FileDataStore ds;
    try {
      //look for a datastore factory to process this file.
      ds = FileDataStoreFinder.getDataStore(f);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return;
    }
    try {
      SimpleFeatureSource featureSource = ds.getFeatureSource();
      SimpleFeatureType schema = featureSource.getSchema();
      //generate a default style (black outline, no fill) for this data
      Style style = SLD.createSimpleStyle(schema);
      String base = FilenameUtils.getBaseName(f.getName());
      // look to see if a matching .sld file exists 
      File sldFile = new File(FilenameUtils.concat(f.getParentFile().getAbsolutePath(), base + ".sld"));
      if (sldFile.exists()) {
        //if SLD file found read it and apply here.
        org.geotools.xml.Configuration configuration = new org.geotools.sld.SLDConfiguration();
        org.geotools.xml.Parser parser = new org.geotools.xml.Parser(configuration);

        // the xml instance document above
        InputStream xml = new FileInputStream(sldFile);

        // parse
        StyledLayerDescriptor sld = (StyledLayerDescriptor) parser.parse(xml);
        style = SLD.defaultStyle(sld);
      }
      //create a maplayer using the data and style 
      FeatureLayer layer = new FeatureLayer(featureSource, style);
      //add it to the map.
      content.addLayer(layer);
    } catch (IOException e) {
      e.printStackTrace();
      return;
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
  }

  
  /**
   * Set the map projection from a string (EPSG:xxxxxx)
   * 
   * @param optionValue a string representing the projection
   */
  public void setProjection(String optionValue) {
    try {
      crs = CRS.decode(optionValue);
    } catch (FactoryException e) {
      e.printStackTrace();
    }

  }

  
  /**
   * Parse a bounding box string (xmin,ymin,xmax,ymax) and set the map to that extent.
   *  
   * @param box comma separated string of bounds
   */
  private void setBbox(String box) {

    String[] c = box.split(",");
    double y2;
    double y1;
    double x2;
    double x1;
    //Make sure to allow for the coordinate system axis order (x might be lat or lon).
    if (crs.getCoordinateSystem().getAxis(0).getDirection().equals(AxisDirection.EAST)) {
      x1 = Double.parseDouble(c[0]);
      x2 = Double.parseDouble(c[2]);
      y1 = Double.parseDouble(c[1]);
      y2 = Double.parseDouble(c[3]);
    } else {
      x1 = Double.parseDouble(c[1]);
      x2 = Double.parseDouble(c[3]);
      y1 = Double.parseDouble(c[0]);
      y2 = Double.parseDouble(c[2]);
    }
    bbox = new ReferencedEnvelope(x1, x2, y1, y2, crs);
    content.getViewport().setBounds(bbox);
  }

  public void view() {

    frame.setVisible(true);
  }

  public static void main(String[] args) throws ParseException {
    TileViewer viewer = new TileViewer(args);

    viewer.view();

  }

  //The following are copied directly from the ImageTutorial 

  /**
   * This method examines the names of the sample dimensions in the provided
   * coverage looking for "red...", "green..." and "blue..." (case insensitive
   * match). If these names are not found it uses bands 1, 2, and 3 for the red,
   * green and blue channels. It then sets up a raster symbolizer and returns
   * this wrapped in a Style.
   *
   * @return a new Style object containing a raster symbolizer set up for RGB
   *         image
   */
  private Style createRGBStyle(GridCoverage2D cov) {
  
    // We need at least three bands to create an RGB style
    int numBands = cov.getNumSampleDimensions();
    if (numBands < 3) {
      return createGreyscaleStyle(1);
    }
    // Get the names of the bands
    String[] sampleDimensionNames = new String[numBands];
    for (int i = 0; i < numBands; i++) {
      GridSampleDimension dim = cov.getSampleDimension(i);
      sampleDimensionNames[i] = dim.getDescription().toString();
    }
  
    int[] channelNum = { -1, -1, -1 };
    // We examine the band names looking for "red...", "green...", "blue...".
    // Note that the channel numbers we record are indexed from 1, not 0.
    for (int i = 0; i < numBands; i++) {
      String name = sampleDimensionNames[i].toLowerCase();
      if (name != null) {
        if (name.matches("red.*")) {
          channelNum[RED] = i + 1;
        } else if (name.matches("green.*")) {
          channelNum[GREEN] = i + 1;
        } else if (name.matches("blue.*")) {
          channelNum[BLUE] = i + 1;
        }
      }
    }
    // If we didn't find named bands "red...", "green...", "blue..."
    // we fall back to using the first three bands in order
    if (channelNum[RED] < 0 || channelNum[GREEN] < 0 || channelNum[BLUE] < 0) {
      channelNum[RED] = 1;
      channelNum[GREEN] = 2;
      channelNum[BLUE] = 3;
    }
    // Now we create a RasterSymbolizer using the selected channels
    SelectedChannelType[] sct = new SelectedChannelType[cov.getNumSampleDimensions()];
    ContrastEnhancement ce = sf.contrastEnhancement(ff.literal(1.0), ContrastMethod.NORMALIZE);
    for (int i = 0; i < 3; i++) {
      sct[i] = sf.createSelectedChannelType(String.valueOf(channelNum[i]), ce);
    }
    RasterSymbolizer sym = sf.getDefaultRasterSymbolizer();
    ChannelSelection sel = sf.channelSelection(sct[RED], sct[GREEN], sct[BLUE]);
    sym.setChannelSelection(sel);
  
    return SLD.wrapSymbolizers(sym);
  }
  
  private Style createGreyscaleStyle(int band) {
    ContrastEnhancement ce = new ContrastEnhancementImpl();// sf.contrastEnhancement(ff.literal(1.0),
                                                           // new Normalize());
    SelectedChannelType sct = sf.createSelectedChannelType(String.valueOf(band), ce);
  
    RasterSymbolizer sym = sf.getDefaultRasterSymbolizer();
    ChannelSelection sel = sf.channelSelection(sct);
    sym.setChannelSelection(sel);
  
    return SLD.wrapSymbolizers(sym);
  }
}